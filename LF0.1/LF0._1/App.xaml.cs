﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace LF0._1
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new LF0._1.MainPage();
        }

        protected override void OnStart()
        {

            //
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
