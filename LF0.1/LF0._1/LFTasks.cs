﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace LF0._1
{
    public class LFTask
    {
        static Dictionary<string,Dictionary<string,List<LFTask>>> _tasksLibrary = new Dictionary<string, Dictionary<string, List<LFTask>>>();
        private static int nOfGroups;
        private static Dictionary<string, int> nOfCountries = new Dictionary<string, int>();
        private static Dictionary<string, List<Country>> listOfVariants = new Dictionary<string, List<Country>>();
        
        static LFTask()
        {
        }
        public static void AddTask(string group, string country, LFTask task)
        {
            //Variants processing
            if (!listOfVariants.ContainsKey(group))
            {
                listOfVariants.Add(group, new List<Country>());
            }
            listOfVariants[group].Add(task.Answer);

            //TasksLib processing
            if (!_tasksLibrary.ContainsKey(group))
            {
                _tasksLibrary.Add(group, new Dictionary<string, List<LFTask>>());
                nOfGroups++;
                nOfCountries.Add(group, 0);
            }
            if (!_tasksLibrary[group].ContainsKey(country))
            {
                _tasksLibrary[group].Add(country, new List<LFTask>());
                nOfCountries[group]++;
            }
            _tasksLibrary[group][country].Add(task);
        }
        public static Task GetRandomTask()
        {
            Random rand = new Random();

            string group = _tasksLibrary.Keys.ToList()[rand.Next(nOfGroups)];
            string country = _tasksLibrary[group].Keys.ToList()[rand.Next(nOfCountries[group])];
            List<LFTask> templist = _tasksLibrary[group][country];
            LFTask task = templist[rand.Next(templist.Count)];
            Country secondVar;
            do
            {
                secondVar = listOfVariants[group][rand.Next(listOfVariants[group].Count)];
            } while (secondVar == task.Answer);
            return new Task(task.Text,secondVar, task.Answer,task.Answer); //
        }


        private string Text { get; }
        private Country Answer { get; }
 

        public LFTask(string text, Country answer)
        {
            Text = text;
            Answer = answer;
        }

    }

    public class Task
    {
        public Country LeftCountry { get; }
        public Country RightCountry { get; }
        public string TaskText { get; }

        private Country correctCountry;
       

        public Task(string taskText, Country firstCountry, Country secondCountry, Country correctCountry)
        {
            Random r = new Random();
            if (r.Next(2) == 0)
            {
                LeftCountry = firstCountry;
                RightCountry = secondCountry;
            }
            else
            {
                LeftCountry = secondCountry;
                RightCountry = firstCountry;
            }
            this.correctCountry = correctCountry;
            TaskText = taskText;
        }

        public bool Check(Country answer) => answer == correctCountry;
       
    }

    public class Country: IComparable
    {
        private static Dictionary<string, Country> listOfCounties = new Dictionary<string, Country>();
        private static int nextId = 0;
        public string Name { get; }
        private int id;
        public Image Image { get; }

        public static Dictionary<string, Country> ListOfCounties => listOfCounties;

        public Country(string name, Image flagPic)
        {
            this.id = nextId;
            nextId++;
            Image = flagPic;
            this.Name = name;        //TODO check name for emptyness or overlenght

            listOfCounties.Add(name, this);
        }

        public int CompareTo(object obj)
        {
            if (((Country) obj).Name == Name)
            {
                return 0;
            }
            return -1;
        }


        public override string ToString() => $"id:{id}, {Name}";
        
    }
}
