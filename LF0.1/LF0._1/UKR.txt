﻿Що було, то мохом поросло.
Що з воза впало, те пропало.
Щоб тебе грім убив і блискавка спалила!
Щоб ти крізь сонце пройшов!
Чужа душа - темний ліс.
Чим далі в ліс, тим більше дров.
Чорна корова, а біле молоко дає.
Чорт біса і з-під купини бачить.
Чи програв, чи виграв, аби свіжі гроші!
Чужому лихові не смійся.
Чуже лихо за ласощі, а своє за хрін.
Чого навчився, того за плечима не носити.
Чужим добром не збагатієш.
Чужими руками добре гада ловити.
Час на часу не стоїть
Хто багато говорить, той мало творить.
Хто рад обіцяти, той не має охоти дати.
Хто про Хому, а хто про Ярему.
Хто діло робить, а хто гави ловить.
Ховається від роботи, як собака від мух.
Хочеш їсти калачі, то не сиди на печі.
Хто літом ледарює, той узимку голодує.
Хто змолоду балує, той під старість старцює.
Хліб на ноги ставить, а горілка з ніг валить.
Хто сміється, тому не минеться.
Хоч кума, але чортом дивиться.
Хоч не нагодувати, аби запрохати.
Хоч мене в гості не звуть, та я знаю, де живуть.
Хоч кіл на голові теши, а він своє!
У доброго коня верстви не довгі.
У кого дочок сім - то й щастя всім, а у мене одна - та й щастя нема.
У ворожки лікуватись - без здоров'я залишатись.
Улесливий чоловік схожий на кішку: спереду ласкає, а ззаду кусає.
У вічі - як лис, а поза очі - як біс.
У кого серце вовче, той їсть, кого схоче.
Ухопив Місяця зубами!
Страх силу одбирає.
Сміливий там знайде, де боягуз загубить.
Сам не б'юсь, а сімох не боюсь.
Слово - не стріла, а ранить глибше.
Слова щирого вітання дорожчі за частування.
Стукотить, гуртотить - комар з дуба летить.
Скромних скрізь поважають, а хвальків зневажають.
Скромність прикрашає людину.
Свого "спасибі" не шкодуй, а чужого не чекай.
Скажи мені, хто твій товариш - тоді я скажу, хто ти.
Стругав, стругав, та й перестругав.
Нехай буде гречка, аби не суперечка!
Назад тільки раки лазять!
Нема гіршого ворога, як дурний розум.
Не купити ума, як нема.
Не одяг красить людину, а добрі справи.
Не так лінь, як не хочеться!
Нічим нічого і не зробиш.
Недаремно говорять, що діло майстра боїться.
На словах міста бере, а на ділі жаби боїться.
Несміливий і своєї частки не одержить.
Не радій чужому горю.
Ніщо не ціниться так дорого і не обходиться так дешево, як ввічливість.
Не знайшовши броду - не лізь у воду.
Не поспішай язиком - поспішай ділом.
Коли пили - гомоніли, а як настав час платити - то всі поніміли.
Краденим добром багатий не будеш.
Крадене не йде на користь.
Коли боїшся, то з чимось таїшся!
Коли б могла, то в ложці води утопила б!
Кричить, як ворона над курчам.
Красавиця, що з-під столу кусається!
Кидається скаженим собакою.
Корова реве, ведмідь реве, а хто кого дере - і чорт не розбере!
Криком дуба не зрубаєш
I будень, і неділя - лінивому все безділля.
I за соломину вхопиться, хто топиться.
Iз рук все валиться в біді.
I граб, і дуб від малої сокири пада.
Iноді рідко, але мітко.
I золота клітка для пташки неволя.
I риба співала б, коли б голос мала!
I коваль, і швець, і кравець, і на дуду грець.
Iз щастя та горя скувалася доля.
I від солодких слів буває гірко.
I кума сором, і хліба жаль.
I тяжко нести, й шкода кинути.
I свиня літала б - та неба не бачить.
Iзнехотя з'їв вовк порося.
I знов за рибу гроші.
Їж з голоду, а люби роботу змолоду.
Заліз, як муха в патоку.
Згинув, як березневий сніг.
Загубилась, як голка в сіні.
Забив, як ведмедя жолудь.
Збирається, як на обід.
З нього толку, як з козла молока.
Добре говорить, але зле робить.
Дивиться лисицею, а думає вовком.
Дивиться, як вовк на козу.
Давали, та з рук не пускали.
Давали, та з кишені не виймали.
Дай курці грядку, а їй і города мало!
Добрався, як кіт до сала!
Дірявого мішка не наповниш.
Договір дорожче за гроші.
Де люблять - не части, де не люблять - не ходи!
Діла на копійку, а балачок на гривню.
Дметься, як пузир на воді.
Далеко куцому до зайця.
Дивиться звисока, а нічого не бачить.
Дай, боже, нашому теляті вовка з'їсти!
Дурний язик голові не приятель.
Далеко йде хороша слава, а худая ще дальше.
До роботи - в Гриця порвані чоботи.
Дай яєчко, облупи та ще й в рот поклади!
Де кисіль - там я й сів, де пиріг - там я й ліг!
Два недужих сіли та й хліб з'їли.
Дарма верба, що груш нема, - аби зазеленіла!
Далеко п'яному до Києва!
В невмілого руки не болять.
Вона співає, як муха в глечику.
Ви по-вашому, а ми по-нашому, а вони по-своєму.
В гості збирайся, а вдома пообідать не забудь!
В чужому глазу бачить зразу, а в своїм - ні разу.
В чужому оці і порошинку бачить, а в своєму і сучка не добачає.
Вовків боятися - в ліс не ходить.
Всіх би перегнав, та бігти боюсь.
Ви утрьох, та злякались вовка, а ми усеми, та тікали від сови.
Вже не в одній чарці денце бачив!
Воду п'ють, а голова з похмілля болить.
Вчи лінивого не молотом, а голодом.
Від ледачого чую, то й не дивую!
В гарячці лежить, а без пам'яті хліб їсть.
В роботі "ох", а їсть за трьох!
Вдача собача: не брехне - то й не дихне.
Вір своїм очам, а не чужим речам.
Від людського поговору не запнешся пеленою.
Великої треба хустки, щоб зав'язати людям усти.
Він набалакає, що й на вербі груші ростуть!
Високо літав, а низько сів.
Величається, як заєць хвостом.
Вертиться, як сорока на тину.
В чуже просо не пхай носа.
Вчепився, як рак!
Ворона маленька, та рот великий.
Він такий, що з вареної крашанки курча висидить!
Він по крашанках пройде і ні одної не роздавить.
Вийшов сухим із води Юхим.
Вовка як не годуй, а він усе в ліс дивиться.
Вовк лисиці не рідня, та повадка одна.
Вовк прийшов у овечій шкурі.
В очі любить, а за очі гудить.
В лиху годину узнаєш вірну людину.
В сльозах ніхто не бачить, а як пісні співають - так чують.
Від малих дітей болить голова, а від великих - серце.
Великі діти - великі й турботи.
Всякій матері свої діти милі.
Від огня, води і злої жони - боже борони!
Вона за ним сохне, а він і не охне.
Волос сивіє, а голова шаліє.
В неї брови до любові, а устоньки до розмови.
Влітку один тиждень рік годує.
Весна днем красна, а на хліб тісна.
Вночі тріщить, а вдень плющить.
Вчення в щасті украшає, а в нещасті утішає.
Впав у біду, як курка в борщ.
Воно як трапиться: коли середа, а коли й п'ятниця.
Вареники-хваленики, усі вас хвалять, та не всі варять.
Вовна, не вовна, аби кишка повна.
Всього буває на віку: по спині, і по боку.
Вертиться, як муха в окропі.
Віділлються вовкові овечі сльози!
В гурті і каша їсться.
Верба хоч і товста, але зсередини пуста.
Вміння і труд все перетруть.
Відстанеш годиною - не здоженеш родиною.
Всякий свого щастя коваль.
Від теплого слова і лід розмерзає.
В здоровому тілі - здоровий дух.
Все купиш, лише тата й мами - ні.
Від лося - лосята, а від свині - поросята.
Всяка пташка своє гніздо знає.
Втікав від вовка, а попав на ведмедя.
Вогонь добрий слуга, але поганий хазяїн.
В чужих руках завжди більший шматок.
Всім по сім, а мені таки вісім.
Вибирала дівка, та вибрала дідька.
Вдача овеча, по-овечому й мекече.
Велика гуля на рівному місці.
Він що кішка: як ти його не кинь, а він все на ноги стає!
Вовка не вбив, а вже шкуру продає.
Високий, як тополя, а дурний, як квасоля.
Гора з горою не зійдеться, чоловік з чоловіком - завжди.
Глибока вода не каламутиться.
Гречана каша хвалилась, ніби вона з коров'ячим маслом народилась.
Городить ні се, ні те.
Гучно, бучно, - а п'яти мерзнуть.
Голосний, як дзвін, а дурний, як довбня!
Грається, як кіт з мишею.
Голова не на те, щоб тільки кашкет носить!
Гарно того вчити, хто хоче все знати.
Горе з дітьми, горе й без дітей.
Густа каша дітей не розгонить.
Гни дерево, поки молоде, учи дітей, поки малі!
Горе тому, в кого нема порядку в дому.
Гість - як невільник: де посадять, там і сидить.
Годуй діда на печі, бо й сам будеш там!
Голос, як сурмонька, але ж чортова думонька.
Говорить, як лисиця, а за пазухою камінь держить.
Гарно колишеш, та сон не бере!
Бачить так, як та сова вночі.
Багато, хоч греблю гати!
Буває, що й черенок блищить.
Батога з піску не сплетеш.
Батогом обуха не переб'єш.
Бачить глаз, а зуб не дістане й раз.
Бовть, мов козел у воду!
Борода по коліна, а розуму , як у дитини
Гірко ковтати, та шкода вертати.
Гречана каша сама себе хвалить.
Говорив Мирон рябої кобили сон!
Гулі не одного в постоли взули.
Горілка без вогню розум спалить.
Горілка з ніг людей збиває.
Горілка - усьому доброму злодійка.
Горілочка - кума, зведе хоч кого з ума!
Господи, за що ти мене караєш: чи я горілки не п'ю, чи я жінки не б'ю, чи я церкви не минаю, чи я в корчмі не буваю?
Гарно сміється той, хто сміється останній!
Гадюка хоч не вкусить, то засичить.
Гляне - молоко кисне. 
Дав дулю через кишеню.
Дорікає горщик чавунові, що чорний; аж гульк - і сам у сажі.
Два коти в одному мішку не помиряться.
Де два сваряться - третій користується.
Добрі гості, та в середу трапились.
Для проханого гостя багато треба, а несподіваний гість - що не постав, те з'їсть.
Де сісти, там сісти, - аби що з'їсти!
Дала борщу такого, що аж туман з моря котиться!
Дурного Кирила і Химка побила.
Дурному свого розуму не вставиш.
Добра кума, та розуму нема.
Засмієшся ти ще й на кутні зуби!
За царя Панька, як була земля тонка, - пальцем ткнеш та й воду п'єш.
За царя Хмеля, як було людей жменя.
За краплю молочка та так бити бичка?
З бика не надоїш молока.
Здобудеш освіту - побачиш більше світу.
З ледарем поведешся - горя наберешся.
Зароблений сухар краще краденого бублика.
Є люди, що й солов'я не люблять.
Є в глечику молоко, та голова не влазить.
Є що їсти й пити, та нема з ким говорити.
Є квас, та не для вас! 
Їв би паляниці, та зубів нема.
Їсть за вола, а робить за комара.
Ївши, гріється, а робивши, мерзне.
Їв би очима, та душа не приймає.
Їдеш у гостину на день - бери хліба на тиждень.
Їжте, очі, хоч повилазьте, - бачили, що купували!
Їздили та возилися і за дуба зачепилися.
Їх сам чорт не розбере!
Їсть, п'є та байдики б'є.
Їхав до Хоми, а потрапив до куми. 
Криком вогню не вгасиш!
Кобила за вовком гналась, та вовкові в зуби попалась.
Коли не пиріг - то й не пирожися, коли не тямиш - то й не берися.
Куди вітер, туди й він.
Краще один мудрий, ніж десять дурних.
Краще з розумним два рази загубити, як з дурним раз знайти.
Кого почитають, того й величають.
Кому честь, тому й хвала.
Кому легко на серці, до того увесь світ сміється.
Краще пізніше, ніж ніколи.
Крий, ховай погане, а воно ж таки гляне!
Кому весілля, а курці смерть.
На правду небагато слів треба.
На дерево дивись, як родить, а на чоловіка, як робить.
Не святі горшки ліплять, а прості люди.
Не місце красить чоловіка, а чоловік місце.
На охочого робочого діло знайдеться.
Не питай старого, а бувалого.
На те й голова, щоб у ній розум був.
Не бажай синові багатства, а бажай розуму!
Не лінися рано вставати та змолоду більше знати!
Навчай інших - і сам навчишся.
Наука в ліс не веде, а з лісу виводить.
На те коня кують, щоб не спотикався.
Не кожна ж Ганна й гарна!
Невесело в світі жити, як нема кого любити. 
Немає третьої клепки в голові.
Носить голову тільки для шапки.
Носиться, як дурень з писаною торбою в будень!
Нема лісу без вовка, а села - без лихого чоловіка.
Не до жартів рибі, коли її під жабри гаком зачепили.
На безриб'ї і рак риба.
На ловця і звір біжить.
На вовка помовка, а заєць капусту з'їв.
Не все так сталося, як жадалось.
Не мала баба клопоту, то купила порося, порося - кувік, а баба в крик. Не страши кота салом.
Не питай, бо старий будеш!
Нова мітла гарно заміта.
Ночвами моря не перепливеш. 
Сім раз відмір, один раз відріж.
Стидно, аж вуха в'януть!
Сів, як чиряк на боці.
Сів, як більмо на оці.
Сидить, як убогий за дверима.
Схопивсь, як ошпарений.
Схожий, як свиня на коня, - тільки шерсть не така.
Скривився, як середа на п'ятницю.
Скаче, як теля на мотузку.
Скільки в лісі пеньків, щоб в тебе було стільки синків!
Сам голий, а сорочка за пазухою.
Своїх не страхай, а наші й так не бояться!
Спасибі за рибу, а за раки нема дяки! 
Сім п'ятниць на тиждень.
Солов'я баснями не годують.
Старається, як мурашка.
Старого горобця на полові не обдуриш.
Сила та розум - краса людини.
Сила без голови шаліє, а розум без сили мліє.
Скільки голів, стільки й умів!
Синиця пищить - зиму віщить.
Сніг, завірюха, бо вже зима коло вуха.
Сумний грудень і в свято, і в будень.
Сім глаз - алмаз, чужі руки - круки. 
Учений шпак говорить всяк.
У нього стільки правди, як у кози хвоста.
У п'яниці коли не під очима синє, так плечі в глині.
Умів красти - умій і очима лупать!
У страха очі великі.
У заздрості на все великі очі.
У чужій руці завжди шматок більший.
У нашому полку чортма толку!
У дурного півня - дурна й пісня.
У нього грошей, як у жаби пір'я.
Умій сказати, умій і змовчати. 
Хоч помирає, а все-таки пальцем киває.
Хоч гірше, аби інше.
Хоч того самого, аби в іншу миску.
Хоч не тільна, то телись.
Хоч не в лад, та широко ступає.
Хотів минути пень, а наїхав на колоду.
Хто в ліс, а хто по дрова поліз.
Хоч у голові пусто, аби грошей густо.
Хоч вдача гаряча, та розум дурний.
Хоч річка й невеличка, а береги ламає.
Хто опарився на окропі, той і холодну воду студить.
Хоч ганьба очі не виїсть, але не дає між люди показатися.
Хто як постеле, так і спатиме.
Чого собі не зичиш, і другому не бажай.
Чуже переступи, та не займай.
Чужий кожух не гріє.
Чорнобрива, як руде теля.
Чим би дитина не бавилась, аби не плакала!
Чоловік у домі - голова, а жінка - душа.
Чоловік без пригоди не проживе.
Швидкий, як черепаха.
Шкода про те говорити, чого не можна зробити.
Шляхом свободи йти - до щастя прийти.
Шануй людей - і тебе шануватимуть. 
Щоб на тобі шкура загорілась!
Щоб тебе буря вивернула!
Що там і говорить, коли нічого й балакать.
Що город - то норов.
Що інше сільце, то інше слівце.
Що інша хатка, то інша гадка.
Що голова, то розум.
Щастя - не підкова, під ногами не знайдеш.
Щастя біжить, а нещастя лежить.
Щастя з нещастям на одних санях їздять.
Щастя має ноги, а біда роги.
Щастя знає, кого шукає.
Щастя розум одбирає, а нещастя повертає. 
Як усе святкувати, то не буде чого ковтати.
Як проголодається, то хліба дістати здогадається.
Як є - розійдеться, а нема - обійдеться.
Як до праці - руки дрижать, а чарочку добре держать!
Як випили варенухи, то й загули, наче мухи.
Як гуляв, так гуляв, - ні чобіт, ні халяв!
Як п'ян - то капітан, а проспиться, - то й свині боїться.
Я йому слово, а він мені десять!
Язиком що хоч мели, а рукам волі не давай!
Як був квас - то не було вас, а як настало квасило - то і вас розносило.
Яке частування, таке й дякування.
Як їдять та п'ють - то й кучерявчиком звуть, а як поп'ють, поїдять, - прощай, шолудяй!
Як часто за шапку береться, так не скоро піде.
Як на пень з'їхав.
Я йому про цибулю, а він мені про часник.
Я йому - вісімнадцять, а він мені - без двох двадцять!
Який Яків, стільки і дяки!
Як заспіває, то й волос в'яне.
Я не я й рука не моя!
Якби все одно, то лазили б у вікно, а то дверей шукають.
Як голова без голови, то й ноги не ходять.
Як з дурнем тягаться, краще відцураться.
Як дурень з печі!
Як напише дурень, то не розбере й розумний.
Як топишся, то й за бритву вхопишся.
Як у лісі гукнеш, так і відгукнеться.
Який стук, такий грюк.
Яке "помогайбі", таке й "доброго здоров'ячка!"
Як захоче коза сіна, то до воза прийде.
Як на гріх, то й граблі стріляють.
Якби знав, де впаду, то й соломки б підстелив!
Як не переплатиш - то й не купиш, як не спустиш - то й не продаси.
Як кота дома нема, то миші по столу бігають.
Як спиться, то не сниться.
Як дають - бери, як б'ють - тікай!
Якби не руки та не ноги, то був би гарний барабан для тривоги.
Як комар наплакав.
Як з гуски вода.
Як з гілля зірвався.
Як руками одняло (хвороба, біль).
Як гороху, тих знайомих: куди не піду, то випхнуть.
Як є голова, то й носи здоров.
Якби та якби, так виросли б і в роті гриби.
Якби у мене було пшоно та сіль, то я б зварив кашу, та жаль, що немає сала.
Якби кури не заклювали, то ще живий би був.
Якось-то бог дасть: батько хату продасть, псів закупить - ніхто не приступить.
Які мамка й татко - таке й насіння.
Як сніг на голову.
Як грім серед ясного неба.
Як їв - то аж упрів, а як працював - то аж задрімав.
Тa пaнич, здaється, його не чув.
Віл був мисливцем, поки не вступив до Нічної Вaрти.
Чи крaще скaзaти, лісокрaдом.
Один чоловік мaв сокиру.
Молодий лицaр повернувся до сивого воякa.
Хочу сaм подивитися нa тих мертв'яків.
Віл був вдячний зa світло.
З переляку в ньому прокинулaся зухвaлість.
Пaн Веймaр Ройс не зізволив нaвіть відповісти.
То булa чудовa зброя зaмкової роботи, зовсім новa нa вигляд.
Зрештою Гaред опустив очі.
Але скрaдaючись догори, Віл не видaвaв aні звуку.
А людей не було.
Лісом ковзaли бліді постaті.
Пaнич поволі обертaвся з мечем нaпоготові, зненaцькa нaшорошившись.
З темряви лісу виниклa тінь і стaлa просто перед Ройсом.
Інший зупинився.
Тоді з тіней виникли тaкі сaмі постaті, як першa.
Блідий меч розрізaв повітря. 
Нa клинку Іншого тaнцювaло бліде блaкитне світло.
Глядaчі присунулися ближче - всі рaзом, нaче їм подaли якийсь знaк.
У смертельній тиші піднялися і опустилися мечі.
Зaлишки мечa Віл знaйшов неподaлік.
Підсудного схопили коло невеликого острогa у горaх.
Бaтько відтяв голову чоловікa одним певним удaром. 
Жесть – це коли ти відчайдушно шукаєш своє місце в житті, а тобі знаходять роботу.
Відверта сповідь грішниці, яка понад усе переймалася чужим болем!
І в яку історію ти знову вплуталася, Руслано?
Але з часом кожна пішла своєю дорогою.
Ці двоє пізнали божественну радість і єднання душ.
її взуття розмірно цокала по бруківці.
Лінь мчить уздовж шосе обганяючи річкові трамваї.
Біжи скільки хочеш, від себе не втечеш.
Мої думки мої скакуни, немов іскри запалять цю ніч.
Мої похмурі дні залишаю долю.
Курчат по осені рахують.
Сьогодні я запізнився на літак.
Спасибі що скачали цю гру.
Не людина прикрашає місце а місце людини.
Не важливо де-ти, важливо хто тебе туди відправив.
Немає нічого краще, ніж чоловік який вміє готувати.
Не роби то чого робити не хотів би.