﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.IO;
using System.Reflection;
using Xamarin.Forms;
using PCLStorage;
namespace LF0._1
{
    public partial class MainPage : ContentPage
    {
        private Random random = new Random();
        private int numOfBackgrounds = 20;
        public MainPage()
        {
            InitializeComponent();
            Initialization();
            MenuMode();
           // GameModeInit();
           
        }

        private void Initialization()
        {  //TODO Localize
           // beachImage.Source = Device.OnPlatform(
           //     iOS: ImageSource.FromFile("Images/waterfront.jpg"),
           //     Android: ImageSource.FromFile("waterfront.jpg"),
           //     WinPhone: ImageSource.FromFile("Images/waterfront.png"));
            new Country("Russian", new Image {Aspect = Aspect.AspectFit, Source = "Pics/Flags/RU.png"});
            new Country("Ukrainian", new Image { Aspect = Aspect.AspectFit, Source = "Pics/Flags/UA.png" });
            new Country("Belarusian", new Image { Aspect = Aspect.AspectFit, Source = "Pics/Flags/BY.png" });

            new Country("English", new Image { Aspect = Aspect.AspectFit, Source = "Pics/Flags/BR.png" });
            new Country("French", new Image { Aspect = Aspect.AspectFit, Source = "Pics/Flags/FR.png" });
            new Country("Italian", new Image { Aspect = Aspect.AspectFit, Source = "Pics/Flags/IT.png" });
            new Country("Spanish", new Image { Aspect = Aspect.AspectFit, Source = "Pics/Flags/SP.png" });

            // TODO Add other countries 
            Debug.WriteLine($"Countries loaded: {Country.ListOfCounties.Count}");
            foreach (Country c in Country.ListOfCounties.Values)
            {
                Debug.WriteLine($"Countries loaded: {c}");
            }
            for (int i = 1; i <= numOfBackgrounds; i++)
            {
                Image backImage = new Image {Aspect = Aspect.Fill, Source = $"Pics/Backgrounds/{i}.jpg"};
                AbsoluteLayout.SetLayoutFlags(backImage, AbsoluteLayoutFlags.All);
                AbsoluteLayout.SetLayoutBounds(backImage, new Rectangle(0f, 0f, 1f, 1f));
                State.Backgrounds.Add(backImage);
            }


            var assembly = typeof(MainPage).GetTypeInfo().Assembly;
            List<Stream> listOfStreams;
            int ind;
            //SLAVIC INIT BLOCK
            listOfStreams = new List<Stream>();
            List<string> slavicCountries = new List<string>{"Russian","Belarusian","Ukrainian"};
            ind = 0;
            listOfStreams.Add(assembly.GetManifestResourceStream("LF0._1.RUS.txt"));
            listOfStreams.Add(assembly.GetManifestResourceStream("LF0._1.BEL.txt"));
            listOfStreams.Add(assembly.GetManifestResourceStream("LF0._1.UKR.txt"));
            //listOfStreams.Add(assembly.GetManifestResourceStream("LF0._1.ENG.txt"));

            //Tasks Initialization
            string text = "";
            foreach (Stream stream in listOfStreams)
            {
                using (var reader = new System.IO.StreamReader(stream))
                {
                    foreach (string line in reader.ReadToEnd().Split('\n'))
                    {
                        LFTask.AddTask("Slavic",slavicCountries[ind],new LFTask(line,Country.ListOfCounties[slavicCountries[ind]]));
                    }
                }
                ind++;
            }
            // END SLAVIC INIT BLOCK
            
            // ROMAN INIT BLOCK TODO ADD ROMAN TEXTS
            listOfStreams = new List<Stream>();
            List<string> romanCountries = new List<string> { "English", "French", "Italian", "Spanish" };
            
            ind = 0;
            listOfStreams.Add(assembly.GetManifestResourceStream("LF0._1.ENG.txt"));
            listOfStreams.Add(assembly.GetManifestResourceStream("LF0._1.FRA.txt"));
            listOfStreams.Add(assembly.GetManifestResourceStream("LF0._1.ITA.txt"));
            listOfStreams.Add(assembly.GetManifestResourceStream("LF0._1.SPA.txt"));

            //Tasks Initialization
            text = "";
            foreach (Stream stream in listOfStreams)
            {
                using (var reader = new System.IO.StreamReader(stream))
                {
                    foreach (string line in reader.ReadToEnd().Split('\n'))
                    {
                        LFTask.AddTask("Roman", romanCountries[ind], new LFTask(line, Country.ListOfCounties[romanCountries[ind]]));
                    }
                }
                Debug.WriteLine(text);
                ind++;
            }
            // END ROMAN INIT BLOCK
            
        }

        public void MenuMode()
        {
            Image background = State.Backgrounds[random.Next(numOfBackgrounds)];
            AbsoluteLayout simpleLayout = new AbsoluteLayout
            {
                BackgroundColor = Color.Green,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            Label startLabel = new Label()
            {
                Text = "Start",
                TextColor = Color.Yellow,
                VerticalTextAlignment =

                    TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
                Font = Font.SystemFontOfSize(NamedSize.Large),
            };
            var tgrstart = new TapGestureRecognizer();
            tgrstart.Tapped += (s, e) =>
            {
                GameModeInit();
            };
            startLabel.GestureRecognizers.Add(tgrstart);
            AbsoluteLayout.SetLayoutFlags(startLabel, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(startLabel, new Rectangle(0.5f, 0.35f, 0.95f, 0.2f));
            simpleLayout.Children.Add(background);
            simpleLayout.Children.Add(startLabel);

            Content = new StackLayout
            {
                Children = {
                    simpleLayout
                }
            };


        }

        public void GameModeInit()
        {
            //TODO Score = 0 and other
            State.Score = "0";
            State.tgr = new TapGestureRecognizer();
            State.tgr.Tapped += (s, e) => {
                if (s.Equals(State.LeftAnswerLabel) || s.Equals(State.LeftButton))
                {

                    if (State.CurrentTask.Check(State.CurrentTask.LeftCountry))
                    {
                        Success();
                    }
                    else
                    {
                        State.LastCorrect = State.CurrentTask.RightCountry;
                        Fail();
                    }
                }
                else if (s.Equals(State.RightAnswerLabel) || s.Equals(State.RightButton))
                {
                    if (State.CurrentTask.Check(State.CurrentTask.RightCountry))
                    {
                        Success();
                    }
                    else
                    {
                        State.LastCorrect = State.CurrentTask.LeftCountry;

                        Fail();
                    }
                }
            };

            foreach (var c in Country.ListOfCounties.Values)
            {
                c.Image.GestureRecognizers.Add(State.tgr);
            }
            GameModeTask();
            
        }
        public void GameModeTask()
        {

            State.CurrentTask = LFTask.GetRandomTask();//GenerateTask();
            
            Image background = State.Backgrounds[random.Next(numOfBackgrounds)];
            State.CurrentBackground = background;
            Image line = new Image { Aspect = Aspect.Fill, Source = "Pics/Other/1.png" };
            Image scoreBack = new Image { Aspect = Aspect.AspectFit, Source = "Pics/Other/4.png" };


            Image leftButton = State.CurrentTask.LeftCountry.Image;
            Image rightButton = State.CurrentTask.RightCountry.Image;
            State.LeftButton = leftButton;
            State.RightButton = rightButton;
            
            Label taskTextLabel = new Label()
            {
                Text = State.CurrentTask.TaskText,
                TextColor =  Color.Yellow,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
                Font = Font.SystemFontOfSize(NamedSize.Large),

            };


            State.LeftAnswerLabel = new Label()
            {
                Text = State.CurrentTask.LeftCountry.Name,
                TextColor = Color.Yellow,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
                Font = Font.SystemFontOfSize(NamedSize.Large),
            };

            State.LeftAnswerLabel.GestureRecognizers.Add(State.tgr);
            State.RightAnswerLabel = new Label()
            {
                Text = State.CurrentTask.RightCountry.Name,
                TextColor = Color.Yellow,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
                Font = Font.SystemFontOfSize(NamedSize.Large),
            };
            State.RightAnswerLabel.GestureRecognizers.Add(State.tgr);

            //TODO make binding
            Label scoreLabel = new Label()
            {
                Text = State.Score,
                TextColor = Color.Yellow,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
                Font = Font.SystemFontOfSize(NamedSize.Large),
            };
            
            AbsoluteLayout simpleLayout = new AbsoluteLayout
            {
                BackgroundColor = Color.Green,
                VerticalOptions = LayoutOptions.FillAndExpand

            };
            
            // PositionProportional flag maps the range (0.0, 1.0) to
            // the range "flush [left|top]" to "flush [right|bottom]"
            //Left Answer
            AbsoluteLayout.SetLayoutFlags(leftButton,AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(leftButton,new Rectangle(0.15f,0.8f, 0.3f,0.3f));

            AbsoluteLayout.SetLayoutFlags(State.LeftAnswerLabel, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(State.LeftAnswerLabel, new Rectangle(0f, 0.7f, 0.5f, 0.3f));

            //Right Answer
            AbsoluteLayout.SetLayoutFlags(rightButton, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(rightButton, new Rectangle(0.85f, 0.8f, 0.3f, 0.3f));

            AbsoluteLayout.SetLayoutFlags(State.RightAnswerLabel, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(State.RightAnswerLabel, new Rectangle(1.0f, 0.7f, 0.5f, 0.3f));

            //Score 
            AbsoluteLayout.SetLayoutFlags(scoreLabel,AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(scoreLabel,new Rectangle(0.5f,0.05f, 0.25f, 0.125f));

            AbsoluteLayout.SetLayoutFlags(scoreBack, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(scoreBack, new Rectangle(0.5f, 0.085f, 0.125f, 0.125f));//BAD 035

            //Task
            AbsoluteLayout.SetLayoutFlags(line, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(line, new Rectangle(0f, 0.2f, 1f, 0.2f));

            AbsoluteLayout.SetLayoutFlags(taskTextLabel, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(taskTextLabel, new Rectangle(0.5f, 0.225f, 0.95f, 0.2f));

            simpleLayout.Children.Add(background);
            simpleLayout.Children.Add(line);
            simpleLayout.Children.Add(taskTextLabel);

            simpleLayout.Children.Add(leftButton);
            simpleLayout.Children.Add(State.LeftAnswerLabel);
            simpleLayout.Children.Add(rightButton);
            simpleLayout.Children.Add(State.RightAnswerLabel);
            
            simpleLayout.Children.Add(scoreBack);
            simpleLayout.Children.Add(scoreLabel);

            Padding =
                new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5);

            // Build the page.
            Content = new StackLayout
            {
                Children = {
                   simpleLayout
                }
            };
          
        }

        public void GameModeGameOver()
        {
            //Score Info
            Image scoreBack = new Image { Aspect = Aspect.AspectFit, Source = "Pics/Other/4.png" };

            Label scoreLabel = new Label()
            {
                Text = State.Score,
                TextColor = Color.Yellow,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
                Font = Font.SystemFontOfSize(NamedSize.Large),
            };

            AbsoluteLayout.SetLayoutFlags(scoreLabel, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(scoreLabel, new Rectangle(0.5f, 0.1f, 0.25f, 0.125f));

            AbsoluteLayout.SetLayoutFlags(scoreBack, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(scoreBack, new Rectangle(0.5f, 0.135f, 0.175f, 0.175f));

            //Task Info
            Image line = new Image { Aspect = Aspect.Fill, Source = "Pics/Other/1.png" };
            Label taskTextLabel = new Label()
            {
                Text = State.CurrentTask.TaskText,
                TextColor = Color.Yellow,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
                Font = Font.SystemFontOfSize(NamedSize.Large),

            };

            AbsoluteLayout.SetLayoutFlags(line, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(line, new Rectangle(0f, 0.35f, 1f, 0.2f));

            AbsoluteLayout.SetLayoutFlags(taskTextLabel, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(taskTextLabel, new Rectangle(0.5f, 0.375f, 0.95f, 0.2f));

            //Correct Country Info
            Image lastAnswerImage = State.LastCorrect.Image;
            Label lastAnswerLabel = new Label()
            {
                Text = State.LastCorrect.Name,
                TextColor = Color.Yellow,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
                Font = Font.SystemFontOfSize(NamedSize.Large),
            };
            AbsoluteLayout.SetLayoutFlags(lastAnswerImage, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(lastAnswerImage, new Rectangle(0.5f, 0.7f, 0.35f, 0.35f));
            AbsoluteLayout.SetLayoutFlags(lastAnswerLabel, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(lastAnswerLabel, new Rectangle(0.5f, 0.6f, 0.5f, 0.5f));

            //Bottom Buttons 
            Label menuLabel = new Label()
            {
                Text = "Menu",
                TextColor = Color.Yellow,
                VerticalTextAlignment =

                    TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
                Font = Font.SystemFontOfSize(NamedSize.Large),
            };
            Label retryLabel = new Label()
            {
                Text = "Retry",
                TextColor = Color.Yellow,
                VerticalTextAlignment =

                    TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
                Font = Font.SystemFontOfSize(NamedSize.Large),
            };

            var tgrGameOver = new TapGestureRecognizer();
            tgrGameOver.Tapped += (s, e) =>
            {
                if (s.Equals(menuLabel))
                {
                    MenuMode();
                }
                else if (s.Equals(retryLabel))
                {
                    GameModeInit();
                }
            };
            menuLabel.GestureRecognizers.Add(tgrGameOver);
            retryLabel.GestureRecognizers.Add(tgrGameOver);

            AbsoluteLayout.SetLayoutFlags(menuLabel, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(menuLabel, new Rectangle(0f, 0.9f, 0.5f, 0.2f));

            AbsoluteLayout.SetLayoutFlags(retryLabel, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(retryLabel, new Rectangle(1f, 0.9f, 0.5f, 0.2f));
            AbsoluteLayout simpleLayout = new AbsoluteLayout
            {
                BackgroundColor = Color.Green,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            simpleLayout.Children.Add(State.CurrentBackground);
            simpleLayout.Children.Add(menuLabel);
            simpleLayout.Children.Add(retryLabel);
            
            simpleLayout.Children.Add(line);
            simpleLayout.Children.Add(taskTextLabel);
            simpleLayout.Children.Add(scoreBack);
            simpleLayout.Children.Add(scoreLabel);
            simpleLayout.Children.Add(lastAnswerImage);
            simpleLayout.Children.Add(lastAnswerLabel);



            Content = new StackLayout
            {
                Children = {
                    simpleLayout
                }
            };

        }

        public void Success()
        {
            State.Score = (int.Parse(State.Score)+1).ToString();
            GameModeTask();
        }

        public void Fail()
        {
            GameModeGameOver();
        }
    }

    static class State
    {
        public static Task CurrentTask;
        public static string Score { get; set; } = "0";
        public static Country LastCorrect { get; set; }
        public static Image LeftButton;
        public static Image RightButton;
        public static Label LeftAnswerLabel;
        public static Label RightAnswerLabel;
        public static Image CurrentBackground { get; set; }

        public static List<Image> Backgrounds = new List<Image>();
        public static TapGestureRecognizer tgr;

    }
}
